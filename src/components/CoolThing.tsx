import * as React from "react";

interface Props {
  coolThing: string;
}

class CoolThing extends React.PureComponent<Props> {
  render() {
    return <div>{this.props.coolThing} is cool!</div>;
  }
}

export default CoolThing;
