import * as React from "react";

import CoolThing from "./CoolThing";

class App extends React.PureComponent {
  render() {
    return (
      <React.Fragment>
        <h1>Oh hai, world!</h1>
        <CoolThing coolThing={"Cool Rick"} />
      </React.Fragment>
    );
  }
}

export default App;
