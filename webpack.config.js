const path = require("path");
const webpack = require("webpack");

const isDev = process.env.NODE_ENV !== "production";
const mode = isDev ? "development" : "production";

// roughly based on https://www.robinwieruch.de/minimal-react-webpack-babel-setup/

module.exports = {
  mode: mode,
  entry: "./src/index.js",
  module: {
    rules: [
      {
        test: /\.(js|ts)x?$/,
        exclude: /node_modules/,
        loader: "babel-loader"
      },
      {
        test: /\.js$/,
        use: ["source-map-loader"],
        enforce: "pre"
      }
    ]
  },
  resolve: {
    extensions: [".js", ".jsx", ".ts", ".tsx", ".json"]
  },
  output: {
    filename: "bundle.js",
    path: path.join(__dirname, "/build")
  },
  plugins: [new webpack.HotModuleReplacementPlugin()],
  devServer: {
    clientLogLevel: "warning",
    contentBase: "./build",
    hot: true,
    stats: {
      colors: true,
      hash: false,
      version: false,
      timings: false,
      assets: false,
      chunks: false,
      modules: false,
      reasons: false,
      children: false,
      source: false,
      errors: true,
      errorDetails: true,
      warnings: true,
      publicPath: false
    }

    // From https://github.com/Kornil/simple-react-app
    // host: "localhost",
    // port: "3000",
    // hot: true,
    // headers: {
    //   "Access-Control-Allow-Origin": "*"
    // },
    // historyApiFallback: true
  }
};
